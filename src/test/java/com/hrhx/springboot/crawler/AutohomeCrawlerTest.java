package com.hrhx.springboot.crawler;

import com.hrhx.springboot.DataSupporterApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = DataSupporterApplication.class)
@RunWith(SpringRunner.class)
public class AutohomeCrawlerTest {
    @Test
    public void test() throws Exception {
        System.setProperty("webdriver.chrome.driver", "/Users/admin/JavaProject/WebCollector/src/main/resources/driver/chromedriver");
        new AutohomeCrawler(AutohomeCrawler.class.getName(),false);
    }
}