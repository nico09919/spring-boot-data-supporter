package com.hrhx.springboot.web;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hrhx.springboot.curve.PolynomialCurveCore;
import com.hrhx.springboot.domain.DataPoint;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
/**
 * 
 * @author duhongming
 *
 */
@Api(value="一元线性回归获取样本数据建立模型",tags="java调用")
@RestController
@RequestMapping(value = "/line")
public class MonadicLinearRegressionController {  
    /**
     * 
     * @param dataPointList
     * @return
     */
    @ApiOperation(value = "一元线性回归获取样本数据建立模型", notes = "[{\"x\": 1,\"y\": 136},{\"x\": 2,\"y\": 143},{\"x\": 3,\"y\": 132},{\"x\": 4,\"y\": 142},{\"x\": 5,\"y\": 147}]")
    @ApiResponses({
        @ApiResponse(code=400,message="请求参数没填好"),
        @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
    })
    @RequestMapping(value = "/monadic", method = RequestMethod.POST)
	public String monadic(@RequestBody List<DataPoint> dataPointList) {
    	Double[][] dataPoints = new Double[dataPointList.size()][2];
    	for(int i=0;i<dataPointList.size();i++){
    		DataPoint dataPoint = dataPointList.get(i);
    		dataPoints[i][0] = dataPoint.getX();
    		dataPoints[i][1] = dataPoint.getY();
    	}
    	String expression = new PolynomialCurveCore().polynomialCurveExpresionAll(dataPoints);
    	return expression;
    } 
  
}  